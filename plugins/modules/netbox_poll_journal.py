#!/usr/bin/python

from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: netbox_poll_journal

short_description: Wait for successful Netbox journal entry

version_added: "0.6.0"

description: |
    Poll journal entries of a Netbox record until a ``success`` or a ``danger``
    entry is found. If the last journal entry is a ``danger`` entry, the module
    will fail with the message of the journal entry.

options:
    netbox_api:
        description: |
            URL to the Netbox API (defaults to the value of the ``NETBOX_API``
            or ``NETBOX_URL`` environment variable).
        required: false
        default: ""
        type: str
    netbox_token:
        description: |
            Token to authenticate with the Netbox API (defaults to the value of
            the ``NETBOX_TOKEN`` environment variable).
        required: false
        default: ""
        type: str
    validate_certs:
        description: Verify SSL certificates.
        required: false
        default: true
        type: bool
    object_type:
        description: The type of the Netbox record associated to the journal entries.
        required: true
        type: str
    object_id:
        description: The ID of the Netbox record associated to the journal entries.
        required: true
        type: int
    comments_filter:
        description: Filter the journal entries by the comments.
        required: false
        default: ""
        type: str
    poll_interval:
        description: The interval in seconds to poll the journal entries (defaults to 10).
        required: false
        default: 10
        type: int

author:
    - DIGIT NMS RPS
"""

EXAMPLES = r"""
- name: Wait for Mapping to be deployed
  ec.rps_nginx.netbox_poll_journal:
    object_type: netbox_rps_plugin.mapping
    object_id: 42
"""

RETURN = r"""
message:
    description: Message of the successful journal entry.
    returned: always
    type: str
    sample: "Success"
"""

from time import sleep
from urllib.parse import urlencode
import json
import os

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url


def run_module():
    env_netbox_url = os.getenv("NETBOX_URL", os.getenv("NETBOX_API", ""))
    env_netbox_token = os.getenv("NETBOX_TOKEN", "")

    module_args = dict(
        netbox_api=dict(type="str", required=False, default=env_netbox_url),
        netbox_token=dict(type="str", required=False, default=env_netbox_token),
        validate_certs=dict(type="bool", required=False, default=True),
        object_type=dict(type="str", required=True),
        object_id=dict(type="int", required=True),
        comments_filter=dict(type="str", required=False, default=""),
        poll_interval=dict(type="int", required=False, default=10),
    )
    result = dict(
        changed=False,
        message="Success",
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)

    if module.check_mode:
        module.exit_json(**result)

    netbox_api = module.params.get("netbox_api", "")
    netbox_token = module.params.get("netbox_token", "")
    validate_certs = module.params.get("validate_certs", False)
    object_type = module.params["object_type"]
    object_id = int(module.params["object_id"])
    comments_filter = module.params.get("comments_filter", "")
    poll_interval = int(module.params.get("poll_interval", 10))

    if not netbox_api or not netbox_token:
        module.fail_json(msg="Missing Netbox credentials")

    def retry(*, retries, delay):
        def wrapper(func):
            def wrapped(*args, **kwargs):
                for iteration in range(retries):
                    try:
                        return func(*args, **kwargs)

                    except Exception:
                        if iteration == retries - 1:
                            raise

                        sleep(delay)

            return wrapped

        return wrapper

    @retry(retries=5, delay=3)
    def fetch_last_journal_entry_kind():
        params = {
            "assigned_object_type": object_type,
            "assigned_object_id": object_id,
            "ordering": "-created",
            "limit": 1,
        }

        if comments_filter:
            params["q"] = comments_filter

        qs = urlencode(params)
        headers = {
            "Authorization": f"Token {netbox_token}",
            "Content-Type": "application/json",
        }

        response, info = fetch_url(
            module,
            f"{netbox_api}/api/extras/journal-entries/?{qs}",
            method="GET",
            headers=headers,
        )

        if info["status"] != 200:
            module.fail_json(**info)

        data = json.loads(response.read())

        if len(data["results"]) == 0:
            return (None, "No journal entries found.")

        else:
            return (
                data["results"][0]["kind"]["value"],
                data["results"][0]["comments"],
            )

    while True:
        kind, msg = fetch_last_journal_entry_kind()
        if kind == "danger":
            module.fail_json(msg=msg)

        elif kind == "success":
            result["message"] = msg
            module.exit_json(**result)

        else:
            sleep(poll_interval)


def main():
    run_module()


if __name__ == "__main__":
    main()
