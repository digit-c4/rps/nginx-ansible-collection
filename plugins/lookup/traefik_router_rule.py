DOCUMENTATION = r"""
    name: traefik_router_rule
    author: DIGIT NMS RPS
    version_added: "0.9.0"
    short_description: Generate Traefik router rule
    description:
        - This lookup plugin computes the Traefik router rule.
        - It will include hosts within the DNS zones of the target environment
        - and exclude hosts within the DNS zones of the other environments if
        - they do not overlap with the target environment's zones.
    options:
        localnet_hostname:
            description: |
                The hostname of the container in the local network.
            required: True
        target_env:
            description: |
                The target environment to generate the router rule for.
            required: True
        all_envs:
            description: |
                The complete list of environments used for the router rule
                generation.
            required: True
        https:
            description: |
                Generate rules for HTTPS traffic.
            type: bool
            default: True
"""

EXAMPLES = r"""
tasks:
   - name: Define the environments
     set_fact:
       envs:
         - name: acceptance
           via: webcloud
           dns_zone: acceptance.mybubble.webcloud.ec.europa.eu
         - name: production
           via: webcloud
           dns_zone: mybubble.webcloud.ec.europa.eu

  - name: Traefik router rules (HTTPS)
    loop: "{{ envs }}"
    debug:
      msg: |
        {{
          lookup(
            'ec.rps_nginx.traefik_router_rule',
            localnet_hostname='mycontainer.localnet',
            target_env=item,
            all_envs=envs,
          )
        }}

  - name: Traefik router rules (HTTP)
    loop: "{{ envs }}"
    debug:
      msg: |
        {{
          lookup(
            'ec.rps_nginx.traefik_router_rule',
            localnet_hostname='mycontainer.localnet',
            target_env=item,
            all_envs=envs,
            https=False,
          )
        }}
"""

RETURN = r"""
    _:
        description:
            - The Traefik router rule for the Docker container.
        type: list
        elements: string
"""

from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        localnet_hostname = kwargs.get("localnet_hostname")
        target_env = kwargs.get("target_env")
        all_envs = kwargs.get("all_envs")
        https = kwargs.get("https", True)

        if not (
            validate_env(target_env)
            and isinstance(all_envs, list)
            and all(validate_env(env) for env in all_envs)
        ):
            raise AnsibleError("Invalid environment definition")

        rule = generate_traefik_router_rule(
            localnet_hostname,
            target_env,
            all_envs,
            https,
        )
        return [rule]  # Ansible expects lookup plugins to return a list


def validate_env(env):
    return (
        isinstance(env, dict)
        and "name" in env
        and "via" in env
        and "dns_zone" in env
        and isinstance(env["name"], str)
        and isinstance(env["via"], str)
        and isinstance(env["dns_zone"], str)
    )


def generate_traefik_router_rule(
    localnet_hostname,
    target_env,
    all_envs,
    https,
):
    other_zones = [
        env["dns_zone"]
        for env in all_envs
        if not is_same_env(env, target_env)
    ]

    cls = HostSNI if https else Host
    rulegen = cls()

    tgt_zone = target_env["dns_zone"]
    local = rulegen.domain(localnet_hostname)
    include = rulegen.rule(tgt_zone)

    excludes = [
        rulegen.rule(zone)
        for zone in other_zones
        if (
            not tgt_zone.endswith(f".{zone}")
            and tgt_zone != zone
        )
    ]

    if len(excludes) > 1:
        excludes = ' || '.join(excludes)
        return f"{local} || ({include} && !({excludes}))"

    elif len(excludes) == 1:
        return f"{local} || ({include} && !{excludes[0]})"

    else:
        return f"{local} || {include}"


def is_same_env(env1, env2):
    return (
        env1["name"] == env2["name"]
        and env1["via"] == env2["via"]
    )


class HostSNI:
    def rule(self, zone):
        return f"({self.domain(zone)} || {self.subdomains(zone)})"


    def domain(self, zone):
        return f"HostSNI(`{zone}`)"


    def subdomains(self, zone):
        escaped_zone = zone.replace(".", r"\.")
        return f"HostSNIRegexp(`^.+\\.{escaped_zone}$`)"


class Host:
    def rule(self, zone):
        return f"({self.domain(zone)} || {self.subdomains(zone)})"


    def domain(self, zone):
        return f"Host(`{zone}`)"


    def subdomains(self, zone):
        escaped_zone = zone.replace(".", r"\.")
        return f"HostRegexp(`^.+\\.{escaped_zone}$`)"
