DOCUMENTATION = r"""
    name: netbox_vm_tags
    author: DIGIT NMS RPS
    version_added: "0.31.0"
    short_description: Generate Netbox Tags for VMs
    description:
        - This lookup plugin computes the tags to add to VMs in Netbox,
        - depending on the RPS environments in the bubble.
    options:
        environments:
            description: |
                The complete list of environments the VMs will serve.
            required: True
        bubble_name:
            description: |
                The name of the bubble.
            required: True
        view_name:
            description: |
                The DNS view of the environments (external, or internal).
            required: True
"""

EXAMPLES = r"""
tasks:
  - name: Define the environments
     set_fact:
       bubble_name: mybubble
       view_name: external
       envs:
         - name: acceptance
           via: webcloud
         - name: production
           via: webcloud

  - name: Netbox Tags to add to VMs
    debug:
      msg: |
        {{
          lookup(
            'ec.rps_nginx.netbox_vm_tags',
            environments=envs,
            bubble_name=bubble_name,
            view_name=view_name,
          )
        }}
"""

RETURN = r"""
    _:
        description:
            - The tags (with name and slug) for VMs in Netbox.
        type: list
        elements: dict
"""

from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase


class LookupModule(LookupBase):
    def run(self, terms, variables=None, **kwargs):
        environments = kwargs.get("environments")
        bubble_name = kwargs.get("bubble_name")
        view_name = kwargs.get("view_name")

        if not all([
            bubble_name,
            view_name,
            isinstance(environments, list),
            all(
                isinstance(env, dict)
                and isinstance(env.get("name"), str)
                and isinstance(env.get("via"), str)
                for env in environments
            ),
        ]):
            raise AnsibleError("Invalid arguments")

        for env in environments:
            env["via"] = env["via"].lower()
            env["name"] = env["name"].lower()

        bubble_name = bubble_name.lower()
        view_name = view_name.lower()

        rps_tags = [
            {
                "name": f"RPS {env['via'].capitalize()} {env['name'].upper()}",
                "slug": f"{bubble_name}_{env['via']}_{env['name']}",
            }
            for env in environments
        ]

        rps_tags.append({
            "name": f"RPS {view_name.capitalize()}",
            "slug": f"{bubble_name}_{view_name}",
        })

        return rps_tags
