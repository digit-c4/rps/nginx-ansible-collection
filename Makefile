v%:
	@sed -i 's/version: \"[0-9]\+\.[0-9]\+\.[0-9]\+\"/version: \"$*\"/g' galaxy.yml
	@sed -i 's/version: v[0-9]\+\.[0-9]\+\.[0-9]\+/version: $@/g' README.md
	@sed -i 's/version: v[0-9]\+\.[0-9]\+\.[0-9]\+/version: $@/g' docs/source/index.rst

proxy/v%:
	@sed -i 's/rps_proxy_docker_image_version: \"v[0-9]\+\.[0-9]\+\.[0-9]\+\"/rps_proxy_docker_image_version: \"v$*\"/g' roles/provision_docker_proxy/defaults/main.yml
	@sed -i 's/``v[0-9]\+\.[0-9]\+\.[0-9]\+``/``v$*``/g' docs/source/roles/provision_docker_proxy.rst

waf/v%:
	@sed -i 's/rps_waf_docker_image_version: \"v[0-9]\+\.[0-9]\+\.[0-9]\+\"/rps_waf_docker_image_version: \"v$*\"/g' roles/provision_docker_waf/defaults/main.yml
	@sed -i 's/``v[0-9]\+\.[0-9]\+\.[0-9]\+``/``v$*``/g' docs/source/roles/provision_docker_waf.rst
	@sed -i 's/rps_waf_docker_image_version: \"v[0-9]\+\.[0-9]\+\.[0-9]\+\"/rps_waf_docker_image_version: \"v$*\"/g' roles/legacy_provision_docker_waf/defaults/main.yml
	@sed -i 's/``v[0-9]\+\.[0-9]\+\.[0-9]\+``/``v$*``/g' docs/source/roles/legacy/provision_docker_waf.rst
