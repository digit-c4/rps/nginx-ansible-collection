Ansible Collection ec.rps_nginx
===============================

.. image:: https://code.europa.eu/digit-c4/rps/nginx-ansible-collection/badges/main/pipeline.svg?style=flat-square
   :target: https://code.europa.eu/digit-c4/rps/nginx-ansible-collection/-/commits/main
   :alt: Pipeline Status

.. image:: https://code.europa.eu/digit-c4/rps/nginx-ansible-collection/-/badges/release.svg?style=flat-square
   :target: https://code.europa.eu/digit-c4/rps/nginx-ansible-collection/-/releases
   :alt: Latest Release

Installation
------------

Add to your ``requirements.yml``:

.. code-block:: yaml

   collections:
     - name: https://code.europa.eu/digit-c4/rps/nginx-ansible-collection.git
       type: git
       version: v0.53.1

Content
-------

.. toctree::
   :maxdepth: 1

   roles/index
   plugins/modules/index
   plugins/lookup/index

See also
--------

* `Docker image for RPS <https://digit-c4.pages.code.europa.eu/rps/proxy/>`_
* `Ansible playbooks for RPS <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-playbooks>`_
* `AWX job provisionning <https://digit-c4.pages.code.europa.eu/awx-data/>`_
