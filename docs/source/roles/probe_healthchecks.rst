Probe healthchecks
==================

This role is used to probe the healthchecks of every virtual host present in the
RPS (2 mappings can be served by the same virtual host). 2 checks are performed:

* A backend test: the test is performed on each instance of the RPS, using the ``Host`` header for routing
* An End-To-End test: the test is performed using the source domain of the mapping (possibly going through a load balancer or a VIP)

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       env_name: 'my-env-name'
     roles:
       - ec.rps_nginx.probe_healthchecks

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "env_name", "Name of the environment to check (the slug of the tag on the mappings and virtual machines)", "N/A"
   "rps_service_name", "Name of service in Netbox associated to the virtual machines", "``nginx``"
