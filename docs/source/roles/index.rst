Ansible Roles
=============

Content
-------

.. toctree::
   :maxdepth: 1

   check_service_catalog
   tag_rps_vms
   provision_docker_router
   provision_docker_proxy
   provision_docker_waf
   provision_docker_c4hou
   provision_docker_httpbin
   probe_healthchecks
   probe_testurls
   notify_teams

Legacy
------

.. toctree::
   :maxdepth: 1

   legacy/compliance
   legacy/deploy_nginx_plus
   legacy/deploy_nginx_waf_modsec
   legacy/apply_policies
   legacy/sid2netbox_migration
   legacy/provision_docker_waf
