Provision Docker C4Hou
======================

This role will setup the C4Hou docker image in Netbox.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       c4hou_virtual_machine: "rps-3.ec.local"
     roles:
       - ec.rps_nginx.provision_docker_c4hou

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

C4Hou-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "c4hou_virtual_machine", "Name of the VM to deploy the container to", "rps-3.ec.local"
   "c4hou_docker_registry_name", "Name of the Docker registry in Netbox", "``code.europa.eu/rps``"
   "c4hou_docker_registry_serveraddress", "Address of the Docker registry", "``https://code.europa.eu:4567/v2/``"
   "c4hou_docker_registry_username", "Username to access the Docker registry", ""
   "c4hou_docker_registry_password", "Password to access the Docker registry", ""
   "c4hou_docker_image_name", "Name of the Docker image to pull", "``code.europa.eu:4567/digit-c4/c4hou``"
   "c4hou_docker_image_version", "Version of the Docker image to pull", "``v1.3.0``"
