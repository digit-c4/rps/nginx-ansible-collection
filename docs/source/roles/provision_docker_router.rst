Provision Docker Router
=======================

This role will setup the Traefik docker image in Netbox for environment traffic
split.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       bubble_name: "mybubble"
       view_name: "internal"
     roles:
       - ec.rps_nginx.provision_docker_router

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

Bubble-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "bubble_name", "Name of the bubble", ""
   "view_name", "Name of the DNS view the container will be (internal/external)", ""

Traefik-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "router_docker_registry", "Name of the Docker registry in Netbox", "``code.europa.eu/lb``"
   "router_docker_registry_serveraddress", "Address of the Docker registry", "``https://code.europa.eu:4567/v2/``"
   "router_docker_registry_username", "Username to access the Docker registry", ""
   "router_docker_registry_password", "Password to access the Docker registry", ""
   "router_docker_image_name", "Name of the Docker image to pull", "``code.europa.eu:4567/digit-c4/lb/traefik``"
   "router_version", "Version of the Docker image to pull", "``v3.2.0-0``"
