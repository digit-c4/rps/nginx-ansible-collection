Provision Docker WAF
====================

This role will setup the Web Application Firewall docker image in Netbox for
legacy environments.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       env_name: "myenv"
     roles:
       - ec.rps_nginx.legacy_provision_docker_waf

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

WAF-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "env_name", "Name of the environment", ""
   "rps_waf_docker_registry", "Name of the Docker registry in Netbox", "``code.europa.eu/waf``"
   "rps_waf_docker_registry_serveraddress", "Address of the Docker registry", "``https://code.europa.eu:4567/v2/``"
   "rps_waf_docker_registry_username", "Username to access the Docker registry", ""
   "rps_waf_docker_registry_password", "Password to access the Docker registry", ""
   "rps_waf_docker_image_name", "Name of the Docker image to pull", "``code.europa.eu:4567/digit-c4/rps/waf/modsec``"
   "rps_waf_docker_image_version", "Version of the Docker image to pull", "``v0.21.1``"
   "rps_waf_enabled", "Wether traffic should be routed to the WAF first", "``true``"
