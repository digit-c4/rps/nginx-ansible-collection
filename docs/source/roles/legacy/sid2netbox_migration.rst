SID to Netbox Migration
=======================

This role fetches mappings from SID and synchronize them in Netbox.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     gather_facts: false
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       env_name: 'my-env-name'
       sid_uid: '12345678'
     roles:
       - ec.rps_nginx.legacy_sid2netbox_migration

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   workspace_name, "Name of the workspace (on the runner)", "``default``"
   sid_env, "Name of the Intragate environment (leave empty for production)", "N/A"
   sid_uid, "Unique identifier of the environment in SID to fetch mappings from", "N/A"
   env_name, "Name of the environment in Netbox to synchronize (the slug of the tag on the mappings and virtual machines)", "N/A"
