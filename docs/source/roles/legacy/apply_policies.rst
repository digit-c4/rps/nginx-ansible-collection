Apply Policies
==============

This role configures the NGINX+ RPS on a Debian 11 virtual machine, to serve
mappings configured in the CMDB.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: all:!localhost
     gather_facts: true
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
     roles:
       - ec.rps_nginx.legacy_apply_policies

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   workspace_name, "Name of the workspace (on the runner)", "``default``"
   gixy_bin, "Absolute path to the ``gixy`` binary", "``/usr/bin/gixy``"
   sid_enabled, "If ``true``, mappings are fetched from SID", "``false``"
   sid_env, "Name of the Intragate environment (leave empty for production)", "N/A"
   sid_uid, "Unique identifier of the environment in SID to fetch mappings from", "N/A"
   sid_custom_nginx_config, "If enabled, add support of custom NGINX+ configuration in SID", "``true``"
   acme_enabled, "If ``true``, will enable the ACME certificate generation", "``false``"
   cert_name, "Name of the wildcard certificate to use when ACME is disabled", "``star-tech``"
   saml_enabled, "If ``true``, will enable the SAML authentication", "``false``"
   modsec_enabled, "If ``true``, will enable the ModSecurity WAF", "``true``"
   rps_mappings, "If SID is disabled, mappings (as fetched from Netbox) to apply", "``[]``"
