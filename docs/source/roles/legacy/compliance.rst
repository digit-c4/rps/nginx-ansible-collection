Virtual machine compliance
==========================

This role ensures that the virtual machine does not have conflicting package or
configuration set up.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: all:!localhost
     gather_facts: true
     roles:
       - ec.rps_nginx.legacy_compliance

Inventory
---------

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   modsec_enabled, "Disable suppression of the ModSecurity WAF if enabled", "``true``"
   appprotect_enabled, "Disable suppression of the AppProtect WAF if enabled", "``false``"
