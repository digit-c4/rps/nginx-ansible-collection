Deploy NGINX+
=============

This role installs the NGINX+ service on a Debian 11 virtual machine.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: all:!localhost
     gather_facts: true
     roles:
       - ec.rps_nginx.legacy_deploy_nginx_plus


**NB:** This role requires an Ansible Vault key.

Inventory
---------

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   workspace_name, "Name of the workspace (on the runner)", "``default``"
   proxy_enabled, "If ``true``, will set the HTTP(S) proxy to the value of the runner's environment variable ``PROXY_EC_URL``", "``true``"
   acme_enabled, "If ``true``, will enable the ACME certificate generation", "``false``"
   cert_name, "Name of the wildcard certificate to use when ACME is disabled", "``star-tech``"
   dns_resolvers, "List of DNS resolvers to use in NGINX+ configuration", "``[]``"
   syslog_tag, "Prefix for NGINX+ logs when redirected to Syslog", "``nginxrps``"
