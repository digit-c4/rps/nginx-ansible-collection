Deploy NGINX+ WAF ModSecurity
=============================

This role installs the Web Application Firewall ModSecurity on a Debian 11
virtual machines.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: all:!localhost
     gather_facts: true
     roles:
       - ec.rps_nginx.legacy_deploy_nginx_waf_modsec

Inventory
---------

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 80, 20

   workspace_name, "Name of the workspace (on the runner)", "``default``"
   proxy_enabled, "If ``true``, will set the HTTP(S) proxy to the value of the runner's environment variable ``PROXY_EC_URL``", "``true``"
   modsec_rules_input_filename, "Name of the file listing the ModSecurity rules to install from the `C4 repository <https://code.europa.eu/digit-c4/modsec-rules.git>`_", "``2.yml``"
