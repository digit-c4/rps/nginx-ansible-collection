Netbox tags manager
====================

This role will create the tags required for deploying RPS and assign them to RPS's VMs.
In case where we have a bubble with only 1,2 or 3VMs, the defualt variables rps_vms_external and rps_vms_internal must be ovveriden in the bubble's variable.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       rps_vms_external: "rps.ec.local"
       rps_vms_internal: "rps.ec.local"
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       bubble_name: "mybubble"
     roles:
       - ec.rps_nginx.tag_rps_vms

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "bubble_name", "Name of the bubble", ""
   "rps_vms_external", "List of RPS virtual machines accessible via EXTERNAL", "rps.ec.local, rps-2.ec.local"
   "rps_vms_internal", "List of RPS virtual machines accessible via INTERNAL", "rps-3.ec.local, rps-4.ec.local"

