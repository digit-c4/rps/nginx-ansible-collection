Probe Testing URLs
==================

This role will check the testing URLs of each mapping.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       env_name: 'my-env-name'
     roles:
       - ec.rps_nginx.probe_testurls

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "env_name", "Name of the environment to check (the slug of the tag on the mappings and virtual machines)", "N/A"
