Provision Docker HTTPBin
========================

This role will setup the `HTTPBin <https://httpbin.org>`_ docker image in Netbox.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       c4hou_virtual_machine: "rps-3.ec.local"
     roles:
       - ec.rps_nginx.provision_docker_httpbin

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

HTTPBin-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "httpbin_virtual_machine", "Name of the VM to deploy the container to", "rps-3.ec.local"
   "httpbin_docker_registry_name", "Name of the Docker registry in Netbox", "``dockerhub``"
   "httpbin_docker_registry_serveraddress", "Address of the Docker registry", "``https://registry.hub.docker.com/v2/``"
   "httpbin_docker_registry_username", "Username to access the Docker registry", ""
   "httpbin_docker_registry_password", "Password to access the Docker registry", ""
   "httpbin_docker_image_name", "Name of the Docker image to pull", "``kennethreitz/httpbin``"
   "httpbin_docker_image_version", "Version of the Docker image to pull", "``latest``"
