Provision Docker Proxy
======================

This role will setup the RPS docker image in Netbox.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       bubble_name: "mybubble"
       rps_view_environments:
         - name: acceptance
           via: intracloud
           dns_zone: "acceptance.mybubble.intracloud.ec.europa.eu"
         - name: production
           via: intracloud
           dns_zone: "mybubble.intracloud.ec.europa.eu"
       rps_deploy_environments:
         - name: acceptance
           via: intracloud
           dns_zone: "acceptance.mybubble.intracloud.ec.europa.eu"
     roles:
       - ec.rps_nginx.provision_docker_proxy

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "bubble_name", "Name of the bubble", ""
   "rps_view_environments", "Environments seen by the router (Traefik)", "``[]``"
   "rps_view_environments[].name", "Name of the environment (ie. test, dev, acceptance, production, ...)", ""
   "rps_view_environments[].via", "Name of the gateway the environment is accessed from (ie. webcloud, intracloud)", ""
   "rps_view_environments[].dns_zone", "DNS zone used by the environment", ""
   "rps_deploy_environments", "Environments to deploy (should be a subset of ``rps_view_environments``)", "``[]``"
   "rps_deploy_environments[].name", "Name of the environment (ie. test, dev, acceptance, production, ...)", ""
   "rps_deploy_environments[].via", "Name of the gateway the environment is accessed from (ie. webcloud, intracloud)", ""
   "rps_deploy_environments[].dns_zone", "DNS zone used by the environment", ""
   "rps_deploy_environments[].proxy_env", "List of extra environment variables to add to the Proxy container", "``[]``"
   "rps_deploy_environments[].proxy_env[].var_name", "Variable name", ""
   "rps_deploy_environments[].proxy_env[].value", "Variable value", ""
   "rps_deploy_environments[].proxy_labels", "List of extra labels to add to the Proxy container", "``[]``"
   "rps_deploy_environments[].proxy_labels[].key", "Label name", ""
   "rps_deploy_environments[].proxy_labels[].value", "Label value", ""
   "rps_deploy_environments[].proxy_binds", "List of extra bind-mounts to add to the Proxy container", "``[]``"
   "rps_deploy_environments[].proxy_binds[].container_path", "Path in the container to mount the bind", ""
   "rps_deploy_environments[].proxy_binds[].host_path", "Path on the host to bind", ""
   "rps_deploy_environments[].agent_allowed_origins", "IP ranges allowed to access the RPS agent API (defaults to global value)", ""
   "rps_deploy_environments[].trusted_ips", "Trusted IP ranges for ``X-Forwarded-For`` header (defaults to global value)", ""
   "rps_proxy_docker_registry", "Name of the Docker registry in Netbox", "``code.europa.eu/rps``"
   "rps_proxy_docker_registry_serveraddress", "Address of the Docker registry", "``https://code.europa.eu:4567/v2/``"
   "rps_proxy_docker_registry_username", "Username to access the Docker registry", ""
   "rps_proxy_docker_registry_password", "Password to access the Docker registry", ""
   "rps_proxy_docker_image_name", "Name of the Docker image to pull", "``code.europa.eu:4567/digit-c4/rps/proxy/nginx-plus``"
   "rps_proxy_docker_image_version", "Version of the Docker image to pull", "``v0.48.7``"
   "rps_agent_allowed_origins", "IP ranges allowed to access the RPS agent API", "``[*]``"
   "rps_trusted_ips", "Trusted IP ranges for ``X-Forwarded-For`` header (usually, the Docker containers IP range)", "``192.168.2.0/24``"
