Check Service Catalog
=====================

This role verifies that the required services are present in the Netbox.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       netbox_dns_service_name: bind
       netbox_syslog_service_name: syslog
     roles:
       - ec.rps_nginx.check_service_catalog

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"
   "netbox_dns_service_name", "Name of the DNS service in Netbox", "``bind``"
   "netbox_syslog_service_name", "Name of the Syslog service in Netbox", "``syslog``"
