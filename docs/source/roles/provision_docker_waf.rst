Provision Docker WAF
====================

This role will setup the Web Application Firewall docker image in Netbox.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       netbox_api: "{{ lookup('env', 'NETBOX_API') }}"
       netbox_token: "{{ lookup('env', 'NETBOX_TOKEN') }}"
       bubble_name: "mybubble"
       rps_view_environments:
         - name: acceptance
           via: intracloud
           dns_zone: "acceptance.mybubble.intracloud.ec.europa.eu"
         - name: production
           via: intracloud
           dns_zone: "mybubble.intracloud.ec.europa.eu"
       rps_deploy_environments:
         - name: acceptance
           via: intracloud
           dns_zone: "acceptance.mybubble.intracloud.ec.europa.eu"
     roles:
       - ec.rps_nginx.provision_docker_waf

Inventory
---------

Netbox-related Inventory
~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "netbox_api", "URL to Netbox", "N/A"
   "netbox_token", "Authentication token to Netbox API", "N/A"

RPS-related Inventory
~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 60, 40

   "bubble_name", "Name of the bubble", ""
   "view_name", "Name of the DNS view the container will be (internal/external)", ""
   "rps_view_environments", "Environments seen by the router (Traefik)", "``[]``"
   "rps_view_environments[].name", "Name of the environment (ie. test, dev, acceptance, production, ...)", ""
   "rps_view_environments[].via", "Name of the gateway the environment is accessed from (ie. webcloud, intracloud)", ""
   "rps_view_environments[].dns_zone", "DNS zone used by the environment", ""
   "rps_deploy_environments", "Environments to deploy (should be a subset of ``rps_view_environments``)", "``[]``"
   "rps_deploy_environments[].name", "Name of the environment (ie. test, dev, acceptance, production, ...)", ""
   "rps_deploy_environments[].via", "Name of the gateway the environment is accessed from (ie. webcloud, intracloud)", ""
   "rps_deploy_environments[].dns_zone", "DNS zone used by the environment", ""
   "rps_deploy_environments[].waf_env", "List of extra environment variables to add to the WAF container", "``[]``"
   "rps_deploy_environments[].waf_env[].var_name", "Variable name", ""
   "rps_deploy_environments[].waf_env[].value", "Variable value", ""
   "rps_deploy_environments[].waf_labels", "List of extra labels to add to the WAF container", "``[]``"
   "rps_deploy_environments[].waf_labels[].key", "Label name", ""
   "rps_deploy_environments[].waf_labels[].value", "Label value", ""
   "rps_deploy_environments[].waf_binds", "List of extra bind-mounts to add to the WAF container", "``[]``"
   "rps_deploy_environments[].waf_binds[].container_path", "Path in the container to mount the bind", ""
   "rps_deploy_environments[].waf_binds[].host_path", "Path on the host to bind", ""
   "rps_deploy_environments[].waf_enabled", "Wether traffic should be routed to the WAF first (defaults to global value)", ""
   "rps_deploy_environments[].waf_mode", "Wether to run the WAF in ``warning`` mode or ``blocking`` mode (defaults to global value)", ""
   "rps_deploy_environments[].waf_auditlog", "Wether to enable the audit log (defaults to global value)", ""
   "rps_deploy_environments[].agent_allowed_origins", "IP ranges allowed to access the RPS agent API (defaults to global value)", ""
   "rps_waf_docker_registry", "Name of the Docker registry in Netbox", "``code.europa.eu/waf``"
   "rps_waf_docker_registry_serveraddress", "Address of the Docker registry", "``https://code.europa.eu:4567/v2/``"
   "rps_waf_docker_registry_username", "Username to access the Docker registry", ""
   "rps_waf_docker_registry_password", "Password to access the Docker registry", ""
   "rps_waf_docker_image_name", "Name of the Docker image to pull", "``code.europa.eu:4567/digit-c4/rps/waf/modsec``"
   "rps_waf_docker_image_version", "Version of the Docker image to pull", "``v0.21.1``"
   "rps_waf_enabled", "Wether traffic should be routed to the WAF first", "``true``"
   "rps_waf_mode", "Wether to run the WAF in ``warning`` mode or ``blocking`` mode", "``blocking``"
   "rps_waf_auditlog", "Wether to enable the audit log", "``true``"
   "rps_agent_allowed_origins", "IP ranges allowed to access the RPS agent API", "``[*]``"
