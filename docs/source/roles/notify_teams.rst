Notify Teams Channel
====================

This role will send a notification to a Teams Channel.

Usage
-----

.. code-block:: yaml

   ---

   - name: My Playbook
     hosts: localhost
     connection: local
     vars:
       notify_teams_webhook_url: "{{ lookup('env', 'TEAMS_WEBHOOK_URL') }}"
     roles:
       - role: ec.rps_nginx.notify_teams
         vars:
           env_name: 'my-env-name'
           notify_teams_webhook_template: !unsafe |
             This is a message from the {{ env_name }} environment.

Inventory
---------

.. csv-table::
   :header: "Key", "Description", "Default Value"
   :widths: 20, 70, 10

   "notify_teams_webhook_url", "URL to the Teams 'Incomming Webhook'", "N/A"
   "notify_teams_webhook_template", "Jinja2 template to use to generate the webhook request body", "N/A"
