---

- name: "Fetch Docker Image from Netbox : {{ task_suffix }}"
  ansible.builtin.set_fact:
    docker_image_response: |
      {{
        [
          lookup(
            'netbox.netbox.nb_lookup',
            'images',
            plugin='docker',
            api_endpoint=netbox_api,
            token=netbox_token,
            validate_certs=False,
            api_filter='name=%s version=%s host_id=%d registry_id=%d' | format(
              rps_waf_docker_image_name,
              rps_waf_docker_image_version,
              docker_host.id,
              docker_registry.id
            )
          )
        ] | flatten
      }}
  register: result
  retries: 5
  delay: 3
  until: result is not failed

- name: "Create Docker Image : {{ task_suffix }}"
  when: docker_image_response | length == 0
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/images/"
    method: POST
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
    body:
      name: "{{ rps_waf_docker_image_name }}"
      version: "{{ rps_waf_docker_image_version }}"
      host: "{{ docker_host.id }}"
      registry: "{{ docker_registry.id }}"
    body_format: json
    status_code: 201
    validate_certs: false
  register: result
  retries: 5
  delay: 3
  until: result is not failed

- name: "Fetch Docker Image information : {{ task_suffix }}"
  when: docker_image_response | length == 0
  ansible.builtin.set_fact:
    docker_image: |
      {{
        [
          lookup(
            'netbox.netbox.nb_lookup',
            'images',
            plugin='docker',
            api_endpoint=netbox_api,
            token=netbox_token,
            validate_certs=False,
            api_filter='name=%s version=%s host_id=%d registry_id=%d' | format(
              rps_waf_docker_image_name,
              rps_waf_docker_image_version,
              docker_host.id,
              docker_registry.id
            )
          )
        ] | flatten
          | first
          | community.general.json_query('value')
      }}
  register: result
  retries: 5
  delay: 3
  until: result is not failed

- name: "Extract Docker Image information : {{ task_suffix }}"
  when: docker_image_response | length > 0
  ansible.builtin.set_fact:
    docker_image: |
      {{
        docker_image_response
          | first
          | community.general.json_query('value')
      }}

- name: "Retry pulling Docker Image : {{ task_suffix }}"
  when: docker_image.ImageID and docker_image.size == 0
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/images/{{ docker_image.id }}/force_pull/"
    method: POST
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
    body:
      name: "{{ docker_image.name }}"
      version: "{{ docker_image.version }}"
      host: "{{ docker_host.id }}"
      registry: "{{ docker_registry.id }}"
    body_format: json
    status_code: 200
    validate_certs: false
  register: result
  retries: 5
  delay: 3
  until: result is not failed

- name: "Wait for Docker Image to be pulled : {{ task_suffix }}"
  when: not docker_image.ImageID
  ec.rps_nginx.netbox_poll_journal:
    netbox_api: "{{ netbox_api }}"
    netbox_token: "{{ netbox_token }}"
    validate_certs: false
    object_type: netbox_docker_plugin.image
    object_id: "{{ docker_image.id }}"
