from argparse import ArgumentParser
from pathlib import Path


class CliArgs:
    sid_data_path: Path
    tag: str


def parse_args() -> CliArgs:
    parser = ArgumentParser()
    parser.add_argument("-s", "--sid-data-path", type=Path)
    parser.add_argument("-t", "--tag", type=str)
    args: CliArgs = parser.parse_args()
    return args
