import requests
import json

from sid2netbox import config
from sid2netbox.data.netbox import NetboxMapping
from sid2netbox.data.patch import MappingsPatch, HeadersPatch


def netbox_sync_mappings(patch: MappingsPatch):
    nb_added = len(patch["added"])
    nb_modified = len(patch["modified"])
    nb_removed = len(patch["removed"])

    print(f"Add {nb_added} new mappings")
    response = requests.post(
        f"{config.NETBOX_URL}/api/plugins/rps/mapping/",
        data=json.dumps(patch["added"]),
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Token {config.NETBOX_TOKEN}",
        },
        verify=False,
    )
    response.raise_for_status()

    print(f"Update {nb_modified} mappings")
    response = requests.patch(
        f"{config.NETBOX_URL}/api/plugins/rps/mapping/",
        data=json.dumps(patch["modified"]),
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Token {config.NETBOX_TOKEN}",
        },
        verify=False,
    )
    response.raise_for_status()

    print(f"Remove {nb_removed} obsolete mappings")
    response = requests.delete(
        f"{config.NETBOX_URL}/api/plugins/rps/mapping/",
        data=json.dumps(patch["removed"]),
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Token {config.NETBOX_TOKEN}",
        },
        verify=False,
    )
    response.raise_for_status()


def netbox_sync_headers(patch: HeadersPatch):
    nb_added = len(patch["added"])
    nb_modified = len(patch["modified"])
    nb_removed = len(patch["removed"])

    print(f"Add {nb_added} new headers")
    response = requests.post(
        f"{config.NETBOX_URL}/api/plugins/rps/http_header/",
        data=json.dumps(patch["added"]),
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Token {config.NETBOX_TOKEN}",
        },
        verify=False,
    )
    response.raise_for_status()

    print(f"Update {nb_modified} headers")
    response = requests.patch(
        f"{config.NETBOX_URL}/api/plugins/rps/http_header/",
        data=json.dumps(patch["modified"]),
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Token {config.NETBOX_TOKEN}",
        },
        verify=False,
    )
    response.raise_for_status()

    print(f"Remove {nb_removed} obsolete headers")
    response = requests.delete(
        f"{config.NETBOX_URL}/api/plugins/rps/http_header/",
        data=json.dumps(patch["removed"]),
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Token {config.NETBOX_TOKEN}",
        },
        verify=False,
    )
    response.raise_for_status()
