from sid2netbox.data.netbox import NetboxMapping, NetboxHeaderInput, NetboxHeaderNested
from sid2netbox.data.sid import SidMapping
from sid2netbox.data.patch import MappingsPatch, HeadersPatch

from . import extract


def proxy_read_timeout(input: str) -> int:
    try:
        timeout = int(input)

    except ValueError:
        timeout = 1

    if not (1 <= timeout <= 300):
        timeout = min(300, max(1, timeout))

    return timeout


def has_mapping_changed(
    sid_mapping: SidMapping,
    netbox_mapping: NetboxMapping,
) -> bool:
    return any([
        sid_mapping["has as source"] != netbox_mapping["source"],
        sid_mapping["has as destination"] != netbox_mapping["target"],
        (sid_mapping["has as testing page"] or None) != netbox_mapping["testingpage"],
        ("ecas" if sid_mapping["is ECAS enabled"] == "true" else "none") != netbox_mapping["authentication"],
        (sid_mapping["uses extended http methods"] == "true") != netbox_mapping["webdav"],
        (sid_mapping["has compression disabled"] != "true") != netbox_mapping["gzip_proxied"],
        (sid_mapping["bypasses the cache"] != "true") != netbox_mapping["proxy_cache"],
        proxy_read_timeout(sid_mapping["has as timeout"] or "") != netbox_mapping["proxy_read_timeout"],
        (5000 if sid_mapping["has server persistence"] == "true" else 1000) != netbox_mapping["keepalive_requests"],
        (300 if sid_mapping["has server persistence"] == "true" else 75) != netbox_mapping["keepalive_timeout"],
    ])


def has_header_changed(
    expected_header: NetboxHeaderInput,
    netbox_header: NetboxHeaderNested,
) -> bool:
    return any([
        expected_header["name"] != netbox_header["name"],
        expected_header["value"] != netbox_header["value"],
        expected_header["apply_to"] != netbox_header["apply_to"],
    ])


def convert_mapping_with_tag(
    sid_mapping: SidMapping,
    tag: str,
) -> NetboxMapping:
    return {
        "source": sid_mapping["has as source"],
        "target": sid_mapping["has as destination"],
        "testingpage": sid_mapping["has as testing page"] or None,
        "authentication": "ecas" if sid_mapping["is ECAS enabled"] == "true" else "none",
        "webdav": sid_mapping["uses extended http methods"] == "true",
        "gzip_proxied": sid_mapping["has compression disabled"] != "true",
        "keepalive_requests": 5000 if sid_mapping["has server persistence"] == "true" else 1000,
        "keepalive_timeout": 300 if sid_mapping["has server persistence"] == "true" else 75,
        "proxy_cache": sid_mapping["bypasses the cache"] != "true",
        "proxy_read_timeout": proxy_read_timeout(sid_mapping["has as timeout"] or ""),
        "client_max_body_size": 128,
        "tags": [
            {"slug": tag}
        ],
        "proxy_buffer_size": 4,
        "proxy_buffer": 16,
        "proxy_busy_buffer": 8,
        "proxy_buffer_responses": "true",
        "proxy_buffer_requests": "true",
        "extra_protocols": [],

    }


def convert_headers_with_tag(
    sid_mapping: SidMapping,
    netbox_mapping_id: int,
    tag: str,
) -> list[NetboxHeaderInput]:
    cpl_actions = sid_mapping["has as CPL action"] or []
    header_actions = filter(
        lambda a: (
            a["type"] == "Header action"
            and a["applies to"] is not None
            and a["is enabled"] == "true"
        ),
        cpl_actions,
    )

    results: list[NetboxHeaderInput] = []

    for header_action in header_actions:
        applies_to, _, header_name = header_action["applies to"].split(".")
        header_value = header_action["has as value"] or ""
        results.append({
            "mapping": netbox_mapping_id,
            "name": header_name,
            "value": header_value,
            "apply_to": applies_to,
            "tags": [
                {"slug": tag}
            ],
        })

    return results


def compute_mappings_patch(
    sid_mappings: list[SidMapping],
    netbox_mappings: list[NetboxMapping],
    tag: str,
) -> MappingsPatch:
    patch: MappingsPatch = {
        "added": [],
        "modified": [],
        "removed": [],
    }

    active_sid_mappings = filter(
        lambda mapping: mapping["deleted"] != "true",
        sid_mappings,
    )

    for sid_mapping in active_sid_mappings:
        netbox_mapping = extract.netbox_find_mapping_by_source(
            netbox_mappings,
            sid_mapping["has as source"],
        )

        if netbox_mapping is None:
            netbox_mapping = convert_mapping_with_tag(sid_mapping, tag)
            patch["added"].append(netbox_mapping)

        elif has_mapping_changed(sid_mapping, netbox_mapping):
            netbox_mapping_id = netbox_mapping["id"]
            netbox_mapping = convert_mapping_with_tag(sid_mapping, tag)
            netbox_mapping["id"] = netbox_mapping_id

            patch["modified"].append(netbox_mapping)

    for netbox_mapping in netbox_mappings:
        sid_mapping = extract.sid_find_mapping_by_source(
            sid_mappings,
            netbox_mapping["source"],
        )

        if sid_mapping is None or sid_mapping["deleted"] == "true":
            patch["removed"].append(netbox_mapping)

    return patch


def compute_headers_patch(
    sid_mappings: list[SidMapping],
    netbox_mappings: list[NetboxMapping],
    netbox_headers: dict[int, list[NetboxHeaderNested]],
    tag: str,
) -> HeadersPatch:
    patch: HeadersPatch = {
        "added": [],
        "modified": [],
        "removed": [],
    }

    active_sid_mappings = filter(
        lambda mapping: mapping["deleted"] != "true",
        sid_mappings,
    )

    for sid_mapping in active_sid_mappings:
        netbox_mapping = extract.netbox_find_mapping_by_source(
            netbox_mappings,
            sid_mapping["has as source"],
        )
        assert netbox_mapping is not None

        expected_headers = convert_headers_with_tag(
            sid_mapping,
            netbox_mapping["id"],
            tag,
        )

        for expected_header in expected_headers:
            netbox_header = None
            for header in netbox_headers.get(netbox_mapping["id"], []):
                if (
                    header["name"] == expected_header["name"]
                    and header["apply_to"] == expected_header["apply_to"]
                ):
                    netbox_header = header
                    break

            if netbox_header is None:
                patch["added"].append(expected_header)

            elif has_header_changed(expected_header, netbox_header):
                expected_header["id"] = netbox_header["id"]
                patch["modified"].append(expected_header)

        mapping_headers = netbox_headers.get(netbox_mapping["id"], [])

        for netbox_header in mapping_headers:
            expected_header = None
            for header in expected_headers:
                if (
                    header["name"] == netbox_header["name"]
                    and header["apply_to"] == netbox_header["apply_to"]
                ):
                    expected_header = header
                    break

            if expected_header is None:
                patch["removed"].append(netbox_header)

    return patch
