from typing import Optional
from pathlib import Path
import requests
import json

from sid2netbox import config
from sid2netbox.data.netbox import NetboxMappingList, NetboxMapping, NetboxHeaderNested
from sid2netbox.data.sid import SidMappingList, SidMapping


def netbox_fetch_mappings_by_tag(tag: str) -> NetboxMappingList:
    response = requests.get(
        f"{config.NETBOX_URL}/api/plugins/rps/mapping/",
        params={
            "tag": tag,
            "limit": 10000,  # a big enough number so we don't have to worry
        },
        headers={
            "Authorization": f"Token {config.NETBOX_TOKEN}",
        },
        verify=False,
    )
    response.raise_for_status()
    return response.json()


def netbox_find_mapping_by_source(
    mappings: list[NetboxMapping],
    source: str,
) -> Optional[NetboxMapping]:
    for mapping in mappings:
        if mapping["source"] == source:
            return mapping

    return None


def netbox_find_headers_per_mapping(
    mappings: list[NetboxMapping],
) -> dict[int, list[NetboxHeaderNested]]:
    headers = {}

    for mapping in mappings:
        headers[mapping["id"]] = mapping["http_headers"]

    return headers


def sid_fetch_mappings_from_path(sid_data_path: Path) -> SidMappingList:
    return json.loads(sid_data_path.read_bytes())


def sid_find_mapping_by_source(
    mappings: list[SidMapping],
    source: str,
) -> Optional[SidMapping]:
    for mapping in mappings:
        if mapping["has as source"] == source:
            return mapping

    return None
