import requests
import sys

from .cli import parse_args
from .etl import extract, transform, load


try:
    args = parse_args()

    sid_api_response = extract.sid_fetch_mappings_from_path(args.sid_data_path)
    netbox_api_response = extract.netbox_fetch_mappings_by_tag(args.tag)
    mappings_patch = transform.compute_mappings_patch(
        sid_api_response["raw_mappings"],
        netbox_api_response["results"],
        args.tag,
    )
    load.netbox_sync_mappings(mappings_patch)

    netbox_api_response = extract.netbox_fetch_mappings_by_tag(args.tag)
    netbox_headers = extract.netbox_find_headers_per_mapping(netbox_api_response["results"])
    headers_patch = transform.compute_headers_patch(
        sid_api_response["raw_mappings"],
        netbox_api_response["results"],
        netbox_headers,
        args.tag,
    )
    load.netbox_sync_headers(headers_patch)

except requests.HTTPError as err:
    print(f"[ERROR] On request: {err.request.method} {err.request.url}", file=sys.stderr)
    print(f"[ERROR] {err}", file=sys.stderr)
    print(f"[ERROR] Response was: {err.response.text}", file=sys.stderr)
    sys.exit(1)
