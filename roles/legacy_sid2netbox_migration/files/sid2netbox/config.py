"""
sid2netbox configuration from environment variables.
"""

from typing import Optional
import os


def config(varname: str, default: Optional[str] = None) -> str:
    """
    Read an environment variable.
    Raise an error if it is not defined and no default is provided.
    """

    val = os.getenv(varname) or default

    if not val:
        raise ValueError(f"{varname} is not set")

    return val


NETBOX_URL = config("NETBOX_URL")
NETBOX_TOKEN = config("NETBOX_TOKEN")
