from typing import Optional, TypedDict, Literal


SidCplAction = TypedDict("SidCplAction", {
    "type": str,
    "is enabled": Optional[Literal["true", "false"]],
    "has as value": Optional[str],
    "applies to": Optional[str],
})


SidMapping = TypedDict("SidMapping", {
    "uid": str,
    "has as source": str,
    "has as destination": str,
    "has as testing page": Optional[str],
    "has as CPL action": Optional[list[SidCplAction]],
    "uses extended http methods": Optional[Literal["true", "false"]],
    "is ECAS enabled": Optional[Literal["true", "false"]],
    "bypasses the cache": Optional[Literal["true", "false"]],
    "has compression disabled": Optional[Literal["true", "false"]],
    "has server persistence": Optional[Literal["true", "false"]],
    "has as timeout": Optional[str],
    "deleted": Optional[Literal["true", "false"]],
})


class SidMappingList(TypedDict):
    raw_mappings: list[SidMapping]
