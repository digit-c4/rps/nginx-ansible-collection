from typing import TypedDict

from .netbox import NetboxMapping, NetboxHeaderInput


class MappingsPatch:
    added: list[NetboxMapping]
    modified: list[NetboxMapping]
    removed: list[NetboxMapping]


class HeadersPatch:
    added: list[NetboxHeaderInput]
    modified: list[NetboxHeaderInput]
    removed: list[NetboxHeaderInput]
