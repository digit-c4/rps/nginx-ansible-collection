from typing import TypedDict, Literal


class NetboxTag(TypedDict):
    slug: str


class NetboxHeaderInput(TypedDict):
    id: int
    mapping: int
    name: str
    value: str
    apply_to: Literal["request", "response"]
    tags: list[NetboxTag]


class NetboxHeaderNested(TypedDict):
    id: int
    name: str
    value: str
    apply_to: Literal["request", "response"]


class NetboxMapping(TypedDict):
    id: int
    source: str
    target: str
    testingpage: str
    webdav: bool
    authentication: str
    gzip_proxied: bool
    keepalive_requests: int
    keepalive_timeout: int
    proxy_cache: bool
    proxy_read_timeout: int
    client_max_body_size: int
    http_headers: list[NetboxHeaderNested]
    tags: list[NetboxTag]
    proxy_buffer_size: int
    proxy_buffer: int
    proxy_busy_buffer: int
    proxy_buffer_responses: bool
    proxy_buffer_requests: bool
    extra_protocols: list[str]




class NetboxMappingList(TypedDict):
    count: int
    results: list[NetboxMapping]
